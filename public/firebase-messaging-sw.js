importScripts('https://www.gstatic.com/firebasejs/7.15.0/firebase-app.js');
importScripts('https://www.gstatic.com/firebasejs/7.15.0/firebase-messaging.js');

firebase.initializeApp({
    apiKey: "AIzaSyARGtYAOlmjpPadAYUtG-qhagSyTxi9NkE",
    authDomain: "docapp-cd0cb.firebaseapp.com",
    projectId: "docapp-cd0cb",
    storageBucket: "docapp-cd0cb.appspot.com",
    messagingSenderId: "424311388981",
    appId: "1:424311388981:web:97f54a9c2e8d9ace286a7c",
    measurementId: "G-J1PMHWYWJL"
});

if (firebase.messaging.isSupported()) {
    firebase.messaging();
}