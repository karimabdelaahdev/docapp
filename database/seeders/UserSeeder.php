<?php

namespace Database\Seeders;

use App\Modules\Users\User;
use Faker\Factory;
use Illuminate\Database\Seeder;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \App\Modules\Users\User::create([
            'name' => 'Super Admin',
            'type' => 'admin',
            'mobile_number' => '01111111111',
            'password' => 'password',
            'is_admin' => 1,
        ]);
//        $users =[];
//        $types =['user' ,'doctor'];
//        for($i=0 ; $i<20 ; $i++){
//            $faker = Factory::create();
//            array_push($users , [
//                'name' => $faker->name,
//                'type' => $types[array_rand($types)],
//                'mobile_number' => '0111111'.rand(1111 , 9999),
//                'password' => 'password',
//                'is_admin' => 0,
//                'is_active' => 0
//            ]);
//        }
//        User::insert($users);
    }
}
