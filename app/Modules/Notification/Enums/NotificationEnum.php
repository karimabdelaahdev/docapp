<?php

namespace App\Modules\Notification\Enums;

use App\Modules\Integration\Models\ApplicationInstallement;
use App\Modules\MortgageApplications\MortgageApplication;
use App\Modules\News\Models\News;
use App\Modules\Products\Models\Product;

abstract class NotificationEnum
{
    /**
     * List of all Work types used in customers table
     */
    public const DATABASE  = "database",
                 EMAIL = "email",
                 FCM = 'fcm'
    ;


    public static function notificationTypes(): array
    {
        return [self::DATABASE , self::EMAIL , self::FCM];
    }
}
