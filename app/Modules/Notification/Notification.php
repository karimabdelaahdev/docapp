<?php

namespace App\Modules\Notification;

use App\Modules\BaseApp\BaseModel;
use App\Modules\Users\User;

class Notification extends BaseModel
{
    protected $table = "notifications";

    protected $fillable = [
        'user_id_from',
        'user_id_to',
        'title',
        'body'
    ];
    public function userFrom()
    {
        return $this->belongsTo(User::class, 'user_id_from');
    }
    public function userTo()
    {
        return $this->belongsTo(User::class, 'user_id_to');
    }
}
