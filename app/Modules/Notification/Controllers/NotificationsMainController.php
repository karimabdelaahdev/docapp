<?php


namespace App\Modules\Notification\Controllers;


use App\Modules\BaseApp\Controllers\BaseController;
use App\Modules\Integration\Controllers\InstallementsServices;
use App\Modules\Integration\Models\ApplicationInstallement;
use App\Modules\Notification\Enums\NotificationEnum;
use App\Modules\Notification\Jobs\SendNotificationEmail;
use App\Modules\Notification\Notification;
use Illuminate\Support\Facades\Log;


class NotificationsMainController extends BaseController
{

    private $user;
    public function __construct()
    {
        $this->user = Auth()->user();
    }
    /**
     * Function  sendNotification
     * to be main main send notification function
     *@param $notifications_types ==>  there 3 types email , database , fcm
     *@param  $data ==> notification data
     *@param bool $is_general ==> flag if this notification is related to user or general notification fo all users
     */
    public function sendNotification($notifications_types , $data)
    {
        if (in_array(NotificationEnum::DATABASE ,$notifications_types)){
            Notification::create($data);
        }
        if (in_array(NotificationEnum::FCM ,$notifications_types)){
                sendFCMNotification($data);
        }
        if (in_array(NotificationEnum::EMAIL ,$notifications_types)){
            if (!empty($data['user_id_to'])){
                SendNotificationEmail::dispatch($data);
            }
        }
    }
}