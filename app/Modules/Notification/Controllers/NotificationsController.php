<?php


namespace App\Modules\Notification\Controllers;


use App\Modules\BaseApp\Controllers\BaseController;
use App\Modules\Notification\Enums\NotificationEnum;
use App\Modules\Notification\Notification;
use App\Modules\Notification\Requests\NotificationsRequest;

class NotificationsController extends BaseController
{
    public $model;
    public $views;
    public $module;

    public function __construct(Notification $model) {
        $this->module = 'notifications';
        $this->views = 'Notification';
        $this->title = trans('app.Notification');
        $this->model = $model;
        $this->rules = $model->rules;
    }

    public function getIndex()
    {
        authorize('view-' . $this->module);
        $data['module'] = $this->module;
        $data['page_title'] = trans('app.List') . " " . $this->title;
        $data['rows'] = $this->model->getData()->orderBy("id","DESC")->paginate();
        return view($this->views . '::index', $data);
    }
    public function getCreate() {
        authorize('create-' . $this->module);
        $data['module'] = $this->module;
        $data['views'] = $this->views;
        $data['page_title'] = trans('app.Create') . " " . $this->title;
        $data['breadcrumb'] = [$this->title => $this->module];
        $data['row'] = $this->model;
        $data['row']->is_active = 1;

        return view($this->views . '::create', $data);
    }

    public function postCreate(NotificationsRequest $request) {

        authorize('create-' . $this->module);
        $data = $request->all();
        $data['general'] = 1;
        if ($row = $this->model->create($data)) {
            $notification_OB = new NotificationsMainController();
            $notification_OB->sendNotification([NotificationEnum::FCM] , $data , 1);
            flash()->success(trans('app.Created successfully'));
            return redirect('/' . $this->module);
        }
        flash()->error(trans('app.failed to save'));
        return back();
    }

    public function getView($id) {
        authorize('view-' . $this->module);
        $data['module'] = $this->module;
        $data['page_title'] = trans('app.View') . " " . $this->title;
        $data['breadcrumb'] = [$this->title => $this->module];
        $data['row'] = $this->model->findOrFail($id);
        return view($this->views . '::view', $data);
    }

}