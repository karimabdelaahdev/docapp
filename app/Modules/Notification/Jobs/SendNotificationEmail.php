<?php

namespace App\Modules\Notification\Jobs;

use App\Modules\Users\User;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\Log;

class SendNotificationEmail implements ShouldQueue {

    use Dispatchable,
        InteractsWithQueue,
        Queueable,
        SerializesModels;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public $row;

    public function __construct($row) {
        $this->row = $row;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle() {
        try {
            $user= User::findOrFail($this->row['user_id_to']);
            $subject  = $this->row['title'];
            $data['email_title'] = $this->row['title'];
            $data['email_body'] = $this->row['body'];
            \Mail::send('Notification::emails.email-notification',
                ['data' => $data, ], function ($mail) use ($user  , $subject) {
                $mail->to($user->email)
                     ->subject($subject);
            });
        } catch (\Throwable $e) {
//            dd('error' , $e->getMessage() , $e->getLine() , $e->getFile());
            \Log::error($e);
        }
    }

}
