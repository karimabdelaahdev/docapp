<?php

namespace App\Modules\Notification\Jobs;

use App\Modules\Notification\Controllers\NotificationsMainController;
use App\Modules\Users\User;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\Log;

class SendNotification implements ShouldQueue {

    use Dispatchable,
        InteractsWithQueue,
        Queueable,
        SerializesModels;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public $types;
    public $data;

    public function __construct($types ,$data) {
        $this->types = $types;
        $this->data = $data;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle() {
        try {
            $notification_OB = new NotificationsMainController();
            $notification_OB->sendNotification($this->types , $this->data);
        } catch (\Throwable $e) {
            \Log::error($e);
        }
    }

}
