<?php

use App\Modules\Users\User;
use LaravelFCM\Facades\FCM;
use LaravelFCM\Message\OptionsBuilder;
use LaravelFCM\Message\PayloadDataBuilder;
use LaravelFCM\Message\PayloadNotificationBuilder;

if (!function_exists('sendFCMNotification')) {
    function sendFCMNotification($payload)
    {
        // send notification to specific user
        sendUserNotification($payload);
    }
}
if (!function_exists('sendUserNotification')) {
    function sendUserNotification($payload)
    {
        if ($payload['user_id_to']) {
            try {
                $optionBuilder = new OptionsBuilder();
                $optionBuilder->setTimeToLive(60 * 20);
                $notificationBuilder = new PayloadNotificationBuilder($payload['title']);
                $notificationBuilder->setBody($payload['body'])
                    ->setSound('default');
                $dataBuilder = new PayloadDataBuilder();
                $dataBuilder->addData([
                    'title' => $payload['title'],
                    'body' => $payload['body'],
                ]);
                $option = $optionBuilder->build();
                $notification = $notificationBuilder->build();
                $data = $dataBuilder->build();
                $user = User::findOrFail($payload['user_id_to']);
                $tokens =[
                    $user->device_token
                ];
                FCM::sendTo($tokens, $option, $notification, $data);
            }catch (\Exception $e){
                dd($e->getMessage());
            }
        }
    }
}
if (!function_exists('getNotificationData')) {
    function getNotificationData(
        $title_en = ' ',
        $title_ar = ' ',
        $body_en = ' ',
        $body_ar = ' ',
        $related_element_id = ' ',
        $related_element_type = ' ',
        $user_id = null,
        $general = false
    ): ?array {
        return [
            'title:en' => $title_en,
            'title:ar' => $title_ar,
            'body:en' => $body_en,
            'body:ar' => $body_ar,
            'related_element_id' => $related_element_id,
            'related_element_type' => $related_element_type,
            'user_id' => $user_id,
            'general' => $general,
        ];
    }
}
