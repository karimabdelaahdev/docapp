<?php

namespace App\Modules\Dashboard\Controllers;

use App\Http\Controllers\Controller;
use App\Modules\Users\User;

class DashboardController extends Controller
{
    public $model;
    public $views;
    public $module;

    public function __construct()
    {
        $this->module = 'dashboard';
        $this->views = 'Dashboard';
    }

    public function getIndex()
    {
        $data['page_title'] = trans('app.Dashboard');
        $data['views'] = $this->views;
        $data['users'] = User::active()->count();

        return view($this->views . '::index', $data);
    }

}
