<?php

namespace App\Modules\HomePage\Controllers;

use App\Http\Controllers\Controller;
use App\Modules\Users\User;

class HomepageController extends Controller
{
    public $model;
    public $views;
    public $module;

    public function __construct()
    {
        $this->module = 'homepage';
        $this->views = 'HomePage';
    }

    public function getIndex()
    {
        $data['views'] = $this->views;
        $data['doctors'] = User::doctor()->active()->get();
        return view($this->views . '::index', $data);
    }

}
