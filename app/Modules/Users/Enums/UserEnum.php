<?php

namespace App\Modules\Users\Enums;

abstract class UserEnum
{
    public static function specialties()
    {
        return [
            'specialty_1',
            'specialty_2',
            'specialty_3',
            'specialty_4',
            'specialty_5',
            'specialty_6',
            'specialty_7',
            'specialty_8',
            'specialty_9',
            'specialty_10',
        ];
    }

    public static function types()
    {
        return [
            'admin',
            'user',
            'doctor',
        ];
    }
    public static function reservationStatuses()
    {
        return [
            'declined',
            'pending',
            'accepted',
            'completed',
        ];
    }

}
