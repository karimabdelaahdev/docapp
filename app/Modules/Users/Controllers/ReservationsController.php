<?php

namespace App\Modules\Users\Controllers;

use App\Http\Controllers\Controller;
use App\Modules\Users\Models\Reservation;
use App\Modules\Users\User;

class ReservationsController extends Controller
{
    public $model;
    public $module, $views;

    public function __construct(Reservation $model)
    {
        $this->views = 'Users::reservations';
        $this->title = trans('app.Reservations');
        $this->model = $model;
    }

    public function getIndex()
    {
        $data['module'] = $this->module;
        $data['views'] = $this->views;
        $request = \request();
        $data['page_title'] = trans('app.List Reservations');
        $data['rows'] = $this->model->where(function ($q) use ($request) {
            if (isset($request->status)) {
                $q->where('status', $request->status);
            }
        })->latest()->paginate();
        return view($this->views . '.index', $data);
    }

    public function toggleActive($id)
    {
        $row = $this->model->findOrFail($id);
        $row->update([
            'is_active' => !$row->is_active
        ]);
        flash()->success(trans('app.Update Successfully'));
        return back();
    }

}
