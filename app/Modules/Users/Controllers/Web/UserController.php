<?php

namespace App\Modules\Users\Controllers\Web;

use App\Http\Controllers\Controller;
use App\Modules\Notification\Controllers\NotificationsMainController;
use App\Modules\Notification\Enums\NotificationEnum;
use App\Modules\Notification\Notification;
use App\Modules\Users\Models\Reservation;
use App\Modules\Users\Requests\CreateUserRequest;
use App\Modules\Users\Requests\UserLoginRequest;
use App\Modules\Users\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;

class UserController extends Controller
{

    private $views;
    private $model;

    public function __construct(User $user)
    {
        $this->views = 'Users::web_users';
        $this->model = $user;
    }

    public function getRegister()
    {
        return view($this->views . '.register');
    }

    public function getLogin()
    {
        return view($this->views . '.login');
    }

    public function postLogin(UserLoginRequest $request)
    {
        $row = $this->model->where('email', trim(request('email')))->where('is_admin', 0)->first();
        if (!$row) {
            $message = trans('auth.There is no account with this email');
            return back()->with(['message' => $message]);
        }
        if (!$row->is_active && $row->type == 'user') {
            $message = trans('auth.This account is banned');
            return back()->with(['message' => $message]);
        }
        if (!Hash::check(trim(request('password')), $row->password)) {
            $message = trans('auth.Trying to login with invalid password');
            return back()->with(['message' => $message]);
        }

        if (Auth::attempt(request()->only('email', 'password'), request('remember_me'))) {
            $row->update([
                'device_token' => $request->device_token
            ]);
            if ($row->type == 'doctor') {
                if (empty($row->session_fees) || empty($row->specialty) || empty($row->location) || empty($row->description)) {
                    return redirect()->route('getCompleteData');
                }
                return redirect()->route('getReservations');
            }
            if (request()->has('to')) {
                return redirect(request('to'));
            }
            return redirect()->intended('/');
        }
        $message = trans('auth.Failed to login');
        return back()->with(['message' => $message]);
    }

    public function postRegister(CreateUserRequest $request)
    {
        $row = $this->model->create(
            $request->except(
                [
                    "_token",
                    "password_confirmation",
                    "/register",
                ]
            )
        );
        $message = trans('app.Created successfully');
        return redirect()->route('getLogin')->with(['success' => $message]);
    }

    public function getLogout()
    {
        auth()->logout();
        return redirect('/');
    }

    public function saveToken(Request $request)
    {
        auth()->user()->update(['device_token' => $request->token]);
        return response()->json(['token saved successfully.']);
    }

    public function sendNotification(Request $request)
    {
        $firebaseToken = User::whereNotNull('device_token')->pluck('device_token')->all();

        $SERVER_API_KEY = 'AAAAYsrt-zU:APA91bHcLlxHb3FzRQz5wnMQCXH5GT2HcF4R4ZoVT2AwymRlbDv8eFW2dmMyI2ZXWbUZhpdP4aDRQw3ndFvq31MDWTi6OPzsO7JM4Q1FH-v5ETZ9fluFnV2btVltkX6F0E9mp881_40m';

        $data = [
            "registration_ids" => $firebaseToken,
            "notification" => [
                "title" => $request->title,
                "body" => $request->body,
            ]
        ];
        $dataString = json_encode($data);

        $headers = [
            'Authorization: key=' . $SERVER_API_KEY,
            'Content-Type: application/json',
        ];

        $ch = curl_init();

        curl_setopt($ch, CURLOPT_URL, 'https://fcm.googleapis.com/fcm/send');
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $dataString);

        $response = curl_exec($ch);

        dd($response);
    }

    public function getUserNotification()
    {
        $data['notifications'] = Notification::where('user_id_to', \auth()->id())
            ->orderBy('id', 'desc')
            ->get();
        return view('Users::notifications', $data);
    }

    public function postReservation(Request $request)
    {
        $row = Reservation::where([
            'user_id' => auth()->id(),
            'doctor_id' => $request->doctor_id,
            'date' => $request->date,
            'specialty' => $request->specialty,
        ])->first();
        if (isset($row)) {
            return redirect()->back()->with(['error' => 'you have and old appointment ']);
        }
        Reservation::create([
            'date' => $request->date,
            'specialty' => $request->specialty,
            'user_id' => auth()->id(),
            'doctor_id' => $request->doctor_id,
            'message' => $request->message
        ]);
        $notification_OB = new NotificationsMainController();
        $notification_OB->sendNotification([
            NotificationEnum::FCM,
            NotificationEnum::DATABASE ,
            NotificationEnum::EMAIL
        ], [
            'user_id_from' => auth()->id(),
            'user_id_to' => $request->doctor_id,
            'title' => 'new reservation from ' . auth()->user()->name,
            'body' => 'you have a new reservation at ' . $request->date . ' from user ' . auth()->user()->name
        ]);
        return redirect()->back()->with(['success' => 'Reservation Sent']);
    }

}
