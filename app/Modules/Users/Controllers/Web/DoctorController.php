<?php

namespace App\Modules\Users\Controllers\Web;

use App\Http\Controllers\Controller;
use App\Modules\Notification\Controllers\NotificationsMainController;
use App\Modules\Notification\Enums\NotificationEnum;
use App\Modules\Users\Models\Reservation;
use App\Modules\Users\User;
use Illuminate\Http\Request;

class DoctorController extends Controller
{

    private $views;
    private $model;

    public function __construct(User $user)
    {
        $this->views = 'Users::doctor';
        $this->model = $user;
    }

    public function getCompleteData()
    {
        if (empty(auth()->user()->session_fees) || empty(auth()->user()->specialty) || empty(
            auth()->user()->location
            ) || empty(auth()->user()->description)) {
            return view($this->views . '.complete_data');
        }
        return redirect()->route('getReservations');
    }

    public function postCompleteData(Request $request)
    {
        $row = $this->model->findOrFail(auth()->id());
        $row->update(
            [
                'session_fees' => $request->session_fees,
                'specialty' => $request->specialty,
                'location' => $request->location,
                'description' => $request->description,
            ]
        );
        return redirect()->route('getReservations');
    }

    public function getReservations()
    {
        if (empty(auth()->user()->session_fees) || empty(auth()->user()->specialty) || empty(
            auth()->user()->location
            ) || empty(auth()->user()->description)) {
            return redirect()->route('getCompleteData');
        }
        $data['rows'] = Reservation::where('doctor_id', auth()->id())->get();
        $data['module']='';
        return view($this->views . '.reservations', $data);
    }

    public function acceptIgnoreReservations($flag, $id)
    {
        $reservation = Reservation::findOrFail($id);
            $reservation->update([
                'status'=> $flag
            ]);
        $notification_OB = new NotificationsMainController();
        $notification_OB->sendNotification([
            NotificationEnum::FCM,
            NotificationEnum::DATABASE ,
            NotificationEnum::EMAIL
        ], [
            'user_id_from' => auth()->id(),
            'user_id_to' => $reservation->user_id,
            'title' => 'doctor ' . auth()->user()->name .' '.$flag.' your reservation',
            'body' => 'doctor ' . auth()->user()->name .' '.$flag.' your reservation with date ' . $reservation->date
        ]);
        return redirect()->back()->with(['success' => 'Reservation '.$flag.' Successfully']);
    }
}
