<?php

namespace App\Modules\Users\Controllers;

use App\Http\Controllers\Controller;
use App\Modules\Users\User;

class UsersController extends Controller
{
    public $model;
    public $module, $views;

    public function __construct(User $model)
    {
        $this->module = 'users';
        $this->views = 'Users::users';
        $this->title = trans('app.Users');
        $this->model = $model;
    }

    public function getIndex()
    {
        $data['module'] = $this->module;
        $data['views'] = $this->views;
        $request = \request();
        $data['page_title'] = trans('app.List Users');
        $data['rows'] = $this->model->where(function ($q) use ($request) {
            $q->where('type', $request->type);
            if (isset($request->is_active)) {
                $q->where('is_active', $request->is_active);
            }
        })->latest()->paginate();
        return view($this->views . '.index', $data);
    }

    public function toggleActive($id)
    {
        $row = $this->model->findOrFail($id);
        $row->update([
            'is_active' => !$row->is_active
        ]);
        flash()->success(trans('app.Update Successfully'));
        return back();
    }

}
