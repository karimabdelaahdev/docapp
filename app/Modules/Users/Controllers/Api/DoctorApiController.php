<?php

namespace App\Modules\Users\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Modules\Users\User;

class DoctorApiController extends Controller
{


    public function __construct(User $user)
    {
        $this->model = $user;
    }

    public function filterDoctorsBySpecialty($specialty)
    {
        return $this->model
            ->where('specialty', $specialty)
            ->active()
            ->doctor()
            ->get();
    }

    public function listAllDoctors()
    {
        $data = $this->model
            ->active()
            ->doctor()
            ->paginate(request()->per_page);
        return response($data ,200);
    }

}
