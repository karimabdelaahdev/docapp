@extends('BaseApp::layouts.web_master')
@section('title')
@endsection
@section('content')
    <section id="about" data-stellar-background-ratio="1">
        <div class="container">
            <div class="row">

                <div class="col-md-12 col-sm-12">
                    <!-- SECTION TITLE -->
                    <div class="section-title wow fadeInUp" data-wow-delay="0.1s">
                        <h2>Notifications</h2>
                    </div>
                    <div class="content">
                        <table class="table display responsive nowrap">
                            <thead>
                            <tr>
                                <th class="wd-15p"># </th>
                                <th class="wd-15p">{{trans('users.Name')}} </th>
                                <th class="wd-15p">{{trans('users.Email')}} </th>
                                <th class="wd-15p">{{trans('users.Mobile')}} </th>
                                <th class="wd-15p">{{trans('users.Title')}} </th>
                                <th class="wd-15p">{{trans('users.Body')}} </th>
                            </tr>
                            </thead>
                            <tbody>
                            @forelse ($notifications as $row)
                                <tr>
                                    <td class="center">{{$row->id}}</td>
                                    <td class="center">{{@$row->userFrom->name}}</td>
                                    <td class="center">{{@$row->userFrom->email}}</td>
                                    <td class="center">{{@$row->userFrom->mobile_number}}</td>
                                    <td class="center">{{@$row->title}}</td>
                                    <td class="center">{{@$row->body}}</td>
                                </tr>
                             @empty
                            @endforelse
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section id="team" data-stellar-background-ratio="1">
    </section>
@endsection
@push('css')
    <style>
        #about {
            background: none !important;
            padding-top: 150px;
            padding-bottom: 200px;
        }
    </style>
@endpush