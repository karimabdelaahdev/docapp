@extends('BaseApp::layouts.master')
@section('title')
    <h6 class="slim-pagetitle">
        {{ @$page_title }}
    </h6>
@endsection
@section('content')
    <div class="section-wrapper">
        @if (!empty($rows))
            <div class="table-responsive">
                <table class="table display responsive nowrap">
                    <thead>
                    <tr>
                        <th class="wd-5p">{{trans('users.ID')}} </th>
                        @if(! request('type'))
                            <th class="wd-10p">{{trans('users.Is Admin')}} </th>
                        @endif
                        <th class="wd-15p">{{trans('users.Name')}} </th>
                        <th class="wd-15p">{{trans('users.Email')}} </th>
                        <th class="wd-15p">{{trans('users.Mobile')}} </th>
                        <th class="wd-10p">{{trans('users.Is Active')}} </th>
                        <th class="wd-15p">{{trans('users.Created at')}}</th>
                        <th class="wd-25p">{{trans('users.Actions')}}</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach ($rows as $row)
                        <tr>
                            <td class="center">{{$row->id}}</td>
                            <td class="center">{{$row->name}}</td>
                            <td class="center">{{$row->email}}</td>
                            <td class="center">{{$row->mobile_number}}</td>
                            <td class="center">{{$row->is_active ? trans('users.Confirmed') : trans('users.Not Confirmed')}}</td>
                            <td class="center">{{$row->created_at}}</td>
                            <td class="center">
                                @if($row->is_active == "1")
                                    <a id="create-edit" class="btn btn-danger btn-xs custom-table-action"
                                       href="/dashboard/{{$module}}/toggle-active/{{$row->id}}" title="Ban">
                                        <i class="fa fa-window-close"></i>
                                    </a>
                                @else
                                    <a id="create-edit" class="btn btn-success btn-xs custom-table-action"
                                       href="/dashboard/{{$module}}/toggle-active/{{$row->id}}" title="Accept">
                                        <i class="fa fa-check-circle"></i>
                                    </a>
                                @endif
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        @else
            {{trans("users.There is no results")}}
        @endif
        <br>
        {{ $rows->links() }}
    </div>
@endsection


@push('js')
    <script>
        $('document').ready(function () {
            var guideStepsArray = [];

            guideStepsArray[0] = {
                target: '#create-button',
                content: "{{trans('help-guide.You can create a new user account for anyone who will frequently use this dashboard')}}"
            };

            guideStepsArray[1] = {
                target: '#create-export',
                content: "{{trans('help-guide.Click on export button to export all users in xlsx file')}}"
            };

            guideStepsArray[2] = {
                target: '#create-view',
                content: "{{trans('help-guide.Want to get details of a specific user, click the View button')}}",
                position: 'bottom'
            };

            guideStepsArray[3] = {
                target: '#create-edit',
                content: "{{trans('help-guide.From here you can edit the user details, this only available for the users created from dashboard')}}",
                position: 'bottom'
            };

            guideStepsArray[4] = {
                target: '#create-delete',
                content: "{{trans('help-guide.Delete button will permanently delete the user, this action is not reversible')}}",
                position: 'bottom'
            };

            var into = new Anno(guideStepsArray);
            $('#help-button').on('click', function () {
                into.show();
            })


        });

    </script>
@endpush
