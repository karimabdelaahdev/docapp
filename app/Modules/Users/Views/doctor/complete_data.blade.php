@extends('BaseApp::layouts.web_master')
@section('title')
@endsection
@section('content')
    <section id="about" data-stellar-background-ratio="1">
        <div class="container">
            <div class="row">

                <div class="col-md-12 col-sm-12">
                    <!-- SECTION TITLE -->
                    <div class="section-title wow fadeInUp" data-wow-delay="0.1s">
                        <h2> Complete Data </h2>
                    </div>
                    @if(Session::has('message'))
                        <div class="alert alert-danger alert-dismissible">
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                            <p>{{ Session::get('message') }}</p>
                        </div>
                    @endif
                    <div class="content">
                        <!-- Nav pills -->
                        <ul class="nav nav-pills" role="tablist">
                            <li class="nav-item">
                                <a class="nav-link active" data-toggle="pill" href="#complete-data">Complete Data</a>
                            </li>
                        </ul>

                        <!-- Tab panes -->
                        <div class="tab-content">
                            <div id="login" class="container tab-pane active">
                                <form method="post" action="{{route('postCompleteData')}}">
                                    @csrf
                                    <div class="form-group">
                                        <label for="exampleFormControlInput1">Session Fees</label>
                                        <input type="number" class="form-control is-valid" id="exampleFormControlInput1"
                                               placeholder="Session Fees" name="session_fees"
                                               value="{{auth()->user()->session_fees}}" required>
                                        @if($errors->has('session_fees'))
                                            <br>
                                            <small id="emailHelp"
                                                   class="form-text text-muted text-danger">{{ $errors->first('session_fees') }}</small>
                                        @endif
                                    </div>
                                    <div class="form-group">
                                        <label for="exampleFormControlInput1">Specialty</label>
                                        <select class="form-control is-valid" name="specialty"
                                                id="exampleFormControlInput2" required>
                                            <option>Select Specialty Here</option>
                                            @forelse(\App\Modules\Users\Enums\UserEnum::specialties() as $specialty)
                                                <option value="{{$specialty}}"
                                                        @if(auth()->user()->specialty == $specialty) selected @endif>{{$specialty}}</option>
                                            @empty
                                            @endforelse

                                        </select>
                                        @if($errors->has('specialty'))
                                            <br>
                                            <small id="emailHelp"
                                                   class="form-text text-muted text-danger">{{ $errors->first('specialty') }}</small>
                                        @endif
                                    </div>
                                    <div class="form-group">
                                        <label for="exampleFormControlInput1">Location</label>
                                        <input type="text" class="form-control is-valid" id="exampleFormControlInput3"
                                               required
                                               placeholder="https://www.google.com/maps/@30.068006,31.3852264,16z"
                                               name="location" value="{{auth()->user()->location}}">
                                        @if($errors->has('location'))
                                            <br>
                                            <small id="emailHelp"
                                                   class="form-text text-muted text-danger">{{ $errors->first('location') }}</small>
                                        @endif
                                    </div>
                                    <div class="form-group">
                                        <label for="exampleFormControlInput1">Description</label>
                                        <textarea class="form-control is-valid"
                                                  id="exampleFormControlInput4"
                                                  required
                                                  name="description"
                                                  placeholder="Lorem Ipsum is simply dummy text of the printing ....">
                                            @if(!empty(auth()->user()->description))
                                                {{auth()->user()->description}}
                                            @endif
                                        </textarea>
                                        @if($errors->has('description'))
                                            <br>
                                            <small id="emailHelp"
                                                   class="form-text text-muted text-danger">{{ $errors->first('description') }}</small>
                                        @endif
                                    </div>

                                    <button type="submit" class="btn btn-primary">Submit</button>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section id="team" data-stellar-background-ratio="1">
    </section>
@endsection
@push('css')
    <style>
        #about {
            background: none !important;
            padding-top: 150px;
            padding-bottom: 200px;
        }

        .content {
            width: 450px;
            height: auto;
            margin: 0 auto;
            padding: 30px;
        }

        .nav-pills {
            width: 450px;
        }

        .nav-item {
            width: 50%;
        }

        .nav-pills .nav-link {
            font-weight: bold;
            padding-top: 13px;
            text-align: center;
            background: #343436;
            color: #fff;
            border-radius: 30px;
            height: 100px;
        }

        .nav-pills .nav-link.active {
            background: #fff;
            color: #000;
        }

        .tab-content {
            position: absolute;
            width: 450px;
            height: auto;
            margin-top: -50px;
            background: #fff;
            color: #000;
            border-radius: 30px;
            z-index: 1000;
            box-shadow: 0px 10px 10px rgba(0, 0, 0, 0.4);
            padding: 30px;
            margin-bottom: 50px;
        }

        .tab-content button {
            border-radius: 15px;
            width: 100px;
            margin: 0 auto;
            float: left;
        }

        .form-control {
            width: auto !important;
        }

        .nav-pills > li + li {
            margin-left: 0px !important;
        }
    </style>
@endpush