@extends('BaseApp::layouts.web_master')
@section('title')
@endsection
@section('content')
    <section id="about" data-stellar-background-ratio="1">
        <div class="container">
            <div class="row">

                <div class="col-md-12 col-sm-12">
                    <!-- SECTION TITLE -->
                    <div class="section-title wow fadeInUp" data-wow-delay="0.1s">
                        <h2>Reservations</h2>
                    </div>
                    @if(Session::has('message'))
                        <div class="alert alert-danger alert-dismissible">
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                            <p>{{ Session::get('message') }}</p>
                        </div>
                    @endif
                    <div class="content">
                        <table class="table display responsive nowrap">
                            <thead>
                            <tr>
                                <th class="wd-15p"># </th>
                                <th class="wd-15p">{{trans('users.Name')}} </th>
                                <th class="wd-15p">{{trans('users.Email')}} </th>
                                <th class="wd-15p">{{trans('users.Mobile')}} </th>
                                <th class="wd-15p">{{trans('users.Reservation Date')}} </th>
                                <th class="wd-15p">{{trans('users.Reservation Message')}} </th>
                                <th class="wd-15p">{{trans('users.Reservation Status')}} </th>
                                <th class="wd-25p">{{trans('users.Actions')}}</th>
                            </tr>
                            </thead>
                            <tbody>
                            @forelse ($rows as $row)
                                <tr>
                                    <td class="center">{{$row->id}}</td>
                                    <td class="center">{{@$row->user->name}}</td>
                                    <td class="center">{{@$row->user->email}}</td>
                                    <td class="center">{{@$row->user->mobile_number}}</td>
                                    <td class="center">{{@$row->date}}</td>
                                    <td class="center">{{@$row->message}}</td>
                                    <td class="center">{{@$row->status}}</td>
                                    <td class="center">
                                        @if($row->status == "pending")
                                            <a id="create-edit" class="btn btn-danger btn-xs custom-table-action"
                                               href="/doctor/accept-ignore-reservations/declined/{{$row->id}}" title="Ban">
                                                <i class="fa fa-window-close"></i>
                                            </a>
                                            <a id="create-edit" class="btn btn-success btn-xs custom-table-action"
                                               href="/doctor/accept-ignore-reservations/accepted/{{$row->id}}" title="Accept">
                                                <i class="fa fa-check-circle"></i>
                                            </a>
                                        @endif
                                    </td>
                                </tr>
                             @empty
                            @endforelse
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section id="team" data-stellar-background-ratio="1">
    </section>
@endsection
@push('css')
    <style>
        #about {
            background: none !important;
            padding-top: 150px;
            padding-bottom: 200px;
        }
    </style>
@endpush