@extends('BaseApp::layouts.master')
@section('title')
    <h6 class="slim-pagetitle">
        {{ @$page_title }}
    </h6>
@endsection
@section('content')
    <div class="section-wrapper">
        @if (!empty($rows))
            <div class="table-responsive">
                <table class="table display responsive nowrap">
                    <thead>
                    <tr>
                        <th class="wd-5p">#</th>
                        <th class="wd-15p">{{trans('users.Doctor')}} </th>
                        <th class="wd-15p">{{trans('users.Doctor Specialty')}} </th>
                        <th class="wd-15p">{{trans('users.Name')}} </th>
                        <th class="wd-15p">{{trans('users.Email')}} </th>
                        <th class="wd-15p">{{trans('users.Mobile')}} </th>
                        <th class="wd-15p">{{trans('users.Reservation Date')}} </th>
                        <th class="wd-15p">{{trans('users.Reservation Message')}} </th>
                        <th class="wd-15p">{{trans('users.Reservation Status')}} </th>
                    </tr>
                    </thead>
                    <tbody>
                    @forelse ($rows as $row)
                        <tr>
                            <td class="center">{{$row->id}}</td>
                            <td class="center">{{@$row->doctor->name}}</td>
                            <td class="center">{{@$row->doctor->specialty}}</td>
                            <td class="center">{{@$row->user->name}}</td>
                            <td class="center">{{@$row->user->email}}</td>
                            <td class="center">{{@$row->user->mobile_number}}</td>
                            <td class="center">{{@$row->date}}</td>
                            <td class="center">{{@$row->message}}</td>
                            <td class="center">{{@$row->status}}</td>
                        </tr>
                    @empty
                    @endforelse
                    </tbody>
                </table>
            </div>
        @else
            {{trans("users.There is no results")}}
        @endif
        <br>
        {{ $rows->links() }}
    </div>
@endsection


@push('js')
    <script>
        $('document').ready(function () {
            var guideStepsArray = [];

            guideStepsArray[0] = {
                target: '#create-button',
                content: "{{trans('help-guide.You can create a new user account for anyone who will frequently use this dashboard')}}"
            };

            guideStepsArray[1] = {
                target: '#create-export',
                content: "{{trans('help-guide.Click on export button to export all users in xlsx file')}}"
            };

            guideStepsArray[2] = {
                target: '#create-view',
                content: "{{trans('help-guide.Want to get details of a specific user, click the View button')}}",
                position: 'bottom'
            };

            guideStepsArray[3] = {
                target: '#create-edit',
                content: "{{trans('help-guide.From here you can edit the user details, this only available for the users created from dashboard')}}",
                position: 'bottom'
            };

            guideStepsArray[4] = {
                target: '#create-delete',
                content: "{{trans('help-guide.Delete button will permanently delete the user, this action is not reversible')}}",
                position: 'bottom'
            };

            var into = new Anno(guideStepsArray);
            $('#help-button').on('click', function () {
                into.show();
            })


        });

    </script>
@endpush
