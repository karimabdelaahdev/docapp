<div class="form-group">
    <label for="InputName">Full Name</label>
    <input type="text" class="form-control is-valid" id="InputName"
           placeholder="Full Name" name="name">
    @if($errors->has('name'))
        <br>
        <small id="emailHelp"
               class="form-text text-muted text-danger">{{ $errors->first('name') }}</small>
    @endif
</div>
<div class="form-group">
    <label for="InputUsername">Mobile Number</label>
    <input type="text" class="form-control is-valid" id="InputUsername"
           placeholder="Mobile Number" name="mobile_number">
    @if($errors->has('mobile_number'))
        <br>
        <small id="emailHelp"
               class="form-text text-muted text-danger">{{ $errors->first('mobile_number') }}</small>
    @endif
</div>
<div class="form-group">
    <label for="exampleFormControlInput2">Email address</label>
    <input type="email" class="form-control is-valid" id="exampleFormControlInput2"
           placeholder="name@example.com" name="email">
    @if($errors->has('email'))
        <br>
        <small id="emailHelp"
               class="form-text text-muted text-danger">{{ $errors->first('email') }}</small>
    @endif
</div>
<div class="form-group">
    <label for="exampleInputPassword2">Password</label>
    <input type="password" class="form-control is-invalid"
           id="exampleInputPassword2" placeholder="Password" name="password">
    <small id="emailHelp" class="form-text text-muted">Password incorrect.</small>
    @if($errors->has('password'))
        <br>
        <small id="emailHelp"
               class="form-text text-muted text-danger">{{ $errors->first('password') }}</small>
    @endif
</div>
<div class="form-group">
    <label for="exampleInputPasswordVer">Verify Password</label>
    <input type="password" class="form-control is-invalid"
           id="exampleInputPasswordVer" placeholder="Password"
           name="password_confirmation">
</div>
<div class="form-group">
    <button type="submit" class="form-control btn btn-primary">Submit</button>
</div>