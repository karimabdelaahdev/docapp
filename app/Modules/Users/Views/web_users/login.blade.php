@extends('BaseApp::layouts.web_master')
@section('title')
@endsection
@section('content')
    <section id="about" data-stellar-background-ratio="1">
        <div class="container">
            <div class="row">

                <div class="col-md-12 col-sm-12">
                    <!-- SECTION TITLE -->
                    <div class="section-title wow fadeInUp" data-wow-delay="0.1s">
                        <h2>Login </h2>
                    </div>
                    @if(Session::has('message'))
                        <div class="alert alert-danger alert-dismissible">
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                            <p>{{ Session::get('message') }}</p>
                        </div>
                    @endif
                    @if(Session::has('success'))
                        <div class="alert alert-success alert-dismissible">
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                            <p>{{ Session::get('success') }}</p>
                        </div>
                    @endif
                    <div class="content">
                        <!-- Nav pills -->
                        <ul class="nav nav-pills" role="tablist">
                            <li class="nav-item">
                                <a class="nav-link active" data-toggle="pill" href="#login">Login</a>
                            </li>
                        </ul>

                    <!-- Tab panes -->
                        <div class="tab-content">
                            <div id="login" class="container tab-pane active">
                                <form method="post" action="{{route('postLogin')}}">
                                    @csrf
                                    <input type="hidden" name="device_token" id="device_token">
                                    <div class="form-group">
                                        <label for="exampleFormControlInput1">Email address</label>
                                        <input type="email" class="form-control is-valid" id="exampleFormControlInput1"
                                               placeholder="name@example.com" name="email">
                                        @if($errors->has('email'))
                                            <br>
                                            <small id="emailHelp"
                                                   class="form-text text-muted text-danger">{{ $errors->first('email') }}</small>
                                        @endif
                                    </div>
                                    <div class="form-group">
                                        <label for="exampleInputPassword1">Password</label>
                                        <input type="password" class="form-control is-invalid"
                                               id="exampleInputPassword1" placeholder="Password" name="password">
                                        @if($errors->has('password'))
                                            <br>
                                            <small id="emailHelp"
                                                   class="form-text text-muted text-danger">{{ $errors->first('password') }}</small>
                                        @endif
                                    </div>
                                    <button type="submit" class="btn btn-primary">Submit</button>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section id="team" data-stellar-background-ratio="1">
    </section>
@endsection
@push('css')
    <style>
        #about {
            background: none !important;
            padding-top: 150px;
            padding-bottom: 200px;
        }

        .content {
            width: 450px;
            height: auto;
            margin: 0 auto;
            padding: 30px;
        }

        .nav-pills {
            width: 450px;
        }

        .nav-item {
            width: 50%;
        }

        .nav-pills .nav-link {
            font-weight: bold;
            padding-top: 13px;
            text-align: center;
            background: #343436;
            color: #fff;
            border-radius: 30px;
            height: 100px;
        }

        .nav-pills .nav-link.active {
            background: #fff;
            color: #000;
        }

        .tab-content {
            position: absolute;
            width: 450px;
            height: auto;
            margin-top: -50px;
            background: #fff;
            color: #000;
            border-radius: 30px;
            z-index: 1000;
            box-shadow: 0px 10px 10px rgba(0, 0, 0, 0.4);
            padding: 30px;
            margin-bottom: 50px;
        }

        .tab-content button {
            border-radius: 15px;
            width: 100px;
            margin: 0 auto;
            float: left;
        }

        .form-control {
            width: auto !important;
        }

        .nav-pills > li + li {
            margin-left: 0px !important;
        }
    </style>
@endpush
@push('js')
    <script src="https://www.gstatic.com/firebasejs/7.23.0/firebase.js"></script>
    <script>
        var firebaseConfig = {
            apiKey: "AIzaSyARGtYAOlmjpPadAYUtG-qhagSyTxi9NkE",
            authDomain: "docapp-cd0cb.firebaseapp.com",
            projectId: "docapp-cd0cb",
            storageBucket: "docapp-cd0cb.appspot.com",
            messagingSenderId: "424311388981",
            appId: "1:424311388981:web:97f54a9c2e8d9ace286a7c",
            measurementId: "G-J1PMHWYWJL"
            // databaseURL: "https://XXXX.firebaseio.com",
        };

        firebase.initializeApp(firebaseConfig);
        const messaging = firebase.messaging();
        // console.log(messaging)
        messaging.requestPermission()
            .then(function () {
                return messaging.getToken()
            })
            .then(function(token) {
                console.log( 'token',token);
                $('#device_token').val(token)
            }).catch(function (err) {
            console.log('User Chat Token Error'+ err);
        });

        messaging.onMessage(function(payload) {
            const noteTitle = payload.notification.title;
            const noteOptions = {
                body: payload.notification.body,
                icon: payload.notification.icon,
            };
            new Notification(noteTitle, noteOptions);
        });

    </script>
@endpush