<?php

namespace App\Modules\Users\Models;

use App\Modules\Users\User;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Reservation extends Model
{
    use HasFactory;
    protected $table ='reservations';
    protected $fillable=[
        'date',
        'specialty',
        'message',
        'user_id',
        'status',
        'doctor_id'
    ];

    public function user()
    {
        return $this->belongsTo(User::class , 'user_id');
    }
    public function doctor()
    {
        return $this->belongsTo(User::class , 'doctor_id');
    }
}
