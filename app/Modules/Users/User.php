<?php

namespace App\Modules\Users;

use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

class User extends Authenticatable
{
    use SoftDeletes, Notifiable;

    protected $table = "users";
    protected $fillable = [
        'name',
        'type',
        'email',
        'mobile_number',
        'session_fees',
        'specialty',
        'description',
        'location',
        'device_token',
        'is_admin',
        'password',
        'is_active',
    ];

    public function setPasswordAttribute($value)
    {
        if (trim($value)) {
            $this->attributes['password'] = bcrypt(trim($value));
        }
    }

    public function scopeActive($query)
    {
        return $query->where('is_active', '=', 1);
    }
    public function scopeDoctor($query)
    {
        return $query->where('type', '=', 'doctor');
    }

    public function scopeNotAdmin($query)
    {
        return $query->where('is_admin', '=', 0);
    }

    public function scopeNotSuperAdmin($query)
    {
        return $query->where('super_admin', '=', 0);
    }

    public function scopeWithoutLoggedUser($query)
    {
        return $query->where('id', '!=', auth()->id());
    }

}
