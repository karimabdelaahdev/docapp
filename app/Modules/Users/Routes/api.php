<?php

use App\Modules\Users\Controllers\Api\DoctorApiController;

Route::get('/filter-doctors-by-specialty/{specialty}', [DoctorApiController::class , 'filterDoctorsBySpecialty']);
Route::get('/list-all-doctors', [DoctorApiController::class , 'listAllDoctors']);



