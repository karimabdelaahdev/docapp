<?php


use App\Modules\Users\Controllers\ReservationsController;
Route::group(['prefix' => '/dashboard/reservations'], function () {
    Route::get('/{status?}', [ReservationsController::class, 'getIndex'])->name('getIndex');
});
