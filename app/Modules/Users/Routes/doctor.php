<?php


use App\Modules\Users\Controllers\Web\DoctorController;

Route::group(['prefix' => 'doctor'], function () {
    Route::get('/complete-data', [DoctorController::class, 'getCompleteData'])->name('getCompleteData');
    Route::post('/complete-data', [DoctorController::class, 'postCompleteData'])->name('postCompleteData');
    Route::get('/reservations', [DoctorController::class, 'getReservations'])->name('getReservations');
    Route::get('/accept-ignore-reservations/{flag}/{id}', [DoctorController::class, 'acceptIgnoreReservations'])->name(
        'acceptIgnoreReservations'
    );
});
