<?php

use App\Modules\Users\Controllers\Web\UserController;

include 'doctor.php';
include 'user.php';
include 'reservations.php';

Route::get('/join-us', [UserController::class, 'getRegister'])->name('joinToUs');
Route::post('/register', [UserController::class, 'postRegister'])->name('postRegister');
Route::get('/login', [UserController::class, 'getLogin'])->name('getLogin');
Route::post('/login', [UserController::class, 'postLogin'])->name('postLogin');
Route::get('/logout', [UserController::class, 'getLogout'])->name('getLogout');
Route::post('/save-token', [UserController::class, 'saveToken'])->name('save-token');
Route::post('/send-notification', [UserController::class, 'sendNotification'])->name('send.notification');
Route::get('/notifications', [UserController::class, 'getUserNotification'])->name('getUserNotification');
Route::post('/post-reservation', [UserController::class, 'postReservation'])->name('postReservation');
