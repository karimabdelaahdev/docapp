<?php

Route::group(['prefix' => '/dashboard/users'], function () {
    Route::get('/', '\App\Modules\Users\Controllers\UsersController@getIndex')->name('users');
    Route::get('/toggle-active/{id}', '\App\Modules\Users\Controllers\UsersController@toggleActive');
});