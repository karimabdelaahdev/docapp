<?php

namespace App\Modules\Users\Requests;

use App\Modules\BaseApp\Requests\BaseAppRequest;
use Illuminate\Validation\Rule;

class CreateUserRequest extends BaseAppRequest
{


    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [
            'name' => 'required|max:191|min:3',
            'email' => [
                'required',
                'nullable',
                Rule::unique('users'),
                'email'
            ],
            'mobile_number' => [
                'required',
                Rule::unique('users')
            ],
            'password' => ['required', 'confirmed']
        ];

        return $rules;
    }
}
