<?php

namespace App\Modules\Users\Requests;

use App\Modules\BaseApp\Requests\BaseAppRequest;

class CompleteDoctorDataRequest extends BaseAppRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'session_fees' => ['required'],
            'location' => ['required'],
            'description' => ['required'],
            'specialty' => ['required']
        ];
    }
}
