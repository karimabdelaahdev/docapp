<li class="nav-item {{(request()->getRequestUri() == "/")?"active":""}}">
    <a class="nav-link" href="{{app()->make("url")->to('/')}}/">
        <i class="icon ion-ios-pie-outline"></i>
        <span>{{trans('navigation.Dashboard')}}</span>
    </a>
</li>
<li id="manage-button"
    class="nav-item with-sub mega-dropdown {{(request()->is('*/pages*' , '*/news*' , '*/countries*' , '*/users*','*/roles*'.'*/options*'))?"active":""}}">
    <a class="nav-link" href="#" data-toggle="dropdown" aria-expanded="true">
        <i class="icon ion-ios-analytics-outline"></i>
        <span> {{trans('navigation.Entities Management')}}</span>
    </a>
    <div class="sub-item">
        <div class="row">
            <div class="col-lg mg-t-30 mg-lg-t-0">
                <div class="row">
                    <div class="col">
                        <label class="section-label">{{trans('navigation.Users')}}</label>
                        <ul>
                            <li class="{{(request()->is('*/patients*'))?"active":""}}">
                                <a href="/dashboard/users?type=user">{{trans('navigation.Patients')}}</a>
                            </li>
                            <li class="{{(request()->is('*/doctors*'))?"active":""}}">
                                <a href="/dashboard/users?type=doctor&is_active=1">{{trans('navigation.Doctors')}}</a>
                            </li>
                            <li class="{{(request()->is('*/doctors*'))?"active":""}}">
                                <a href="/dashboard/users?type=doctor&is_active=0">{{trans('navigation.Non Active Doctors')}}</a>
                            </li>
                        </ul>
                    </div>
                    <div class="col">
                        <label class="section-label">{{trans('navigation.Reservations')}}</label>
                        <ul>
                            <li class="{{(request()->is('*/reservations*'))?"active":""}}">
                                <a href="/dashboard/reservations">{{trans('navigation.All Reservations')}}</a>
                            </li>
                            <li class="{{(request()->is('*/reservations*'))?"active":""}}">
                                <a href="/dashboard/reservations?status=declined">{{trans('navigation.Declined Reservations')}}</a>
                            </li>
                            <li class="{{(request()->is('*/reservations*'))?"active":""}}">
                                <a href="/dashboard/reservations?status=pending">{{trans('navigation.Pending Reservations')}}</a>
                            </li>
                            <li class="{{(request()->is('*/reservations*'))?"active":""}}">
                                <a href="/dashboard/reservations?status=accepted">{{trans('navigation.Accepted Reservations')}}</a>
                            </li>
                            <li class="{{(request()->is('*/reservations*'))?"active":""}}">
                                <a href="/dashboard/reservations?status=completed">{{trans('navigation.Completed Reservations')}}</a>
                            </li>

                        </ul>
                    </div>
                </div>
            </div>


        </div>
    </div>
</li>