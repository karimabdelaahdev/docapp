<!DOCTYPE html>
<html lang="en">
<head>

    <title>Health - Medical Website Template</title>
    <!--

    Template 2098 Health

    http://www.tooplate.com/view/2098-health

    -->
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=Edge">
    <meta name="description" content="">
    <meta name="keywords" content="">
    <meta name="author" content="Tooplate">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">

    <link rel="stylesheet" href="/web/css/bootstrap.min.css">
    <link rel="stylesheet" href="/web/css/font-awesome.min.css">
    <link rel="stylesheet" href="/web/css/animate.css">
    <link rel="stylesheet" href="/web/css/owl.carousel.css">
    <link rel="stylesheet" href="/web/css/owl.theme.default.min.css">
    <link rel="icon" href="" type="image/x-icon"/>

    <!-- MAIN CSS -->
    <link rel="stylesheet" href="/web/css/tooplate-style.css">
    @stack('css')
</head>
<body id="top" data-spy="scroll" data-target=".navbar-collapse" data-offset="50">

<!-- PRE LOADER -->
<section class="preloader">
    <div class="spinner">

        <span class="spinner-rotate"></span>

    </div>
</section>


<!-- HEADER -->
<header>
    <div class="container">
        <div class="row">

            <div class="col-md-4 col-sm-5">
                <p>Welcome to a Professional Health Care</p>
            </div>

            <div class="col-md-8 col-sm-7 text-align-right">
                <span class="phone-icon"><i class="fa fa-phone"></i> 010-060-0160</span>
                <span class="date-icon"><i class="fa fa-calendar-plus-o"></i> 6:00 AM - 10:00 PM (Mon-Fri)</span>
                <span class="email-icon"><i class="fa fa-envelope-o"></i> <a href="#">info@company.com</a></span>
            </div>

        </div>
    </div>
</header>


<!-- MENU -->
<section class="navbar navbar-default navbar-static-top" role="navigation">
    <div class="container">

        <div class="navbar-header">
            <button class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                <span class="icon icon-bar"></span>
                <span class="icon icon-bar"></span>
                <span class="icon icon-bar"></span>
            </button>

            <!-- lOGO TEXT HERE -->
            <a href=" @if(auth()->user() && auth()->user()->type == 'doctor'){{route('getReservations')}} @else / @endif"
               class="navbar-brand"><i class="fa fa-h-square"></i>ealth Center</a>
        </div>

        <!-- MENU LINKS -->
        <div class="collapse navbar-collapse">
            <ul class="nav navbar-nav navbar-right">
                @if(auth()->user())
                    @if(auth()->user()->type == 'doctor')
                        <li><a href="{{route('getReservations')}}" class="smoothScroll">Reservations</a></li>
                        <li><a href="{{route('getUserNotification')}}" class="smoothScroll">
                                <i class="fa fa-bell"></i>
                            </a></li>
                    @else
                        <li><a href="/" class="smoothScroll">Home</a></li>
                        <li><a href="#about" class="smoothScroll">About Us</a></li>
                        <li><a href="#team" class="smoothScroll">Doctors</a></li>
                        <li><a href="#news" class="smoothScroll">News</a></li>
                        <li><a href="{{route('getUserNotification')}}" class="smoothScroll">
                                <i class="fa fa-bell"></i>
                            </a></li>
                        <li class="appointment-btn"><a href="/#appointment">Make an appointment</a></li>
                    @endif

                    <li><a href="{{route('getLogout')}}" class="smoothScroll">Logout</a></li>

                @else
                    <li><a href="/" class="smoothScroll">Home</a></li>
                    <li><a href="#about" class="smoothScroll">About Us</a></li>
                    <li><a href="#team" class="smoothScroll">Doctors</a></li>
                    <li><a href="#news" class="smoothScroll">News</a></li>
                    <li class="appointment-btn"><a href="{{route('joinToUs')}}">Join Us</a></li>
                    <li><a href="{{route('getLogin')}}" class="smoothScroll">Login</a></li>
                @endif
            </ul>
        </div>

    </div>
</section>

@yield('content')
<!-- FOOTER -->
<footer data-stellar-background-ratio="5">
    <div class="container">
        <div class="row">

            <div class="col-md-4 col-sm-4">
                <div class="footer-thumb">
                    <h4 class="wow fadeInUp" data-wow-delay="0.4s">Contact Info</h4>
                    <p>Fusce at libero iaculis, venenatis augue quis, pharetra lorem. Curabitur ut dolor eu elit
                        consequat ultricies.</p>

                    <div class="contact-info">
                        <p><i class="fa fa-phone"></i> 010-070-0170</p>
                        <p><i class="fa fa-envelope-o"></i> <a href="#">info@company.com</a></p>
                    </div>
                </div>
            </div>

            <div class="col-md-4 col-sm-4">
                <div class="footer-thumb">
                    <h4 class="wow fadeInUp" data-wow-delay="0.4s">Latest News</h4>
                    <div class="latest-stories">
                        <div class="stories-image">
                            <a href="#"><img src="/web/images/news-image.jpg" class="img-responsive" alt=""></a>
                        </div>
                        <div class="stories-info">
                            <a href="#"><h5>Amazing Technology</h5></a>
                            <span>March 08, 2018</span>
                        </div>
                    </div>

                    <div class="latest-stories">
                        <div class="stories-image">
                            <a href="#"><img src="/web/images/news-image.jpg" class="img-responsive" alt=""></a>
                        </div>
                        <div class="stories-info">
                            <a href="#"><h5>New Healing Process</h5></a>
                            <span>February 20, 2018</span>
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-md-4 col-sm-4">
                <div class="footer-thumb">
                    <div class="opening-hours">
                        <h4 class="wow fadeInUp" data-wow-delay="0.4s">Opening Hours</h4>
                        <p>Monday - Friday <span>06:00 AM - 10:00 PM</span></p>
                        <p>Saturday <span>09:00 AM - 08:00 PM</span></p>
                        <p>Sunday <span>Closed</span></p>
                    </div>

                    <ul class="social-icon">
                        <li><a href="https://www.facebook.com/tooplate" class="fa fa-facebook-square"
                               attr="facebook icon"></a></li>
                        <li><a href="#" class="fa fa-twitter"></a></li>
                        <li><a href="#" class="fa fa-instagram"></a></li>
                    </ul>
                </div>
            </div>

            <div class="col-md-12 col-sm-12 border-top">
                <div class="col-md-4 col-sm-6">
                    <div class="copyright-text">
                        <p>Copyright &copy; 2017 Your Company

                            | Design: <a href="http://www.tooplate.com" target="_parent">Tooplate</a></p>
                    </div>
                </div>
                <div class="col-md-6 col-sm-6">
                    <div class="footer-link">
                        <a href="#">Laboratory Tests</a>
                        <a href="#">Departments</a>
                        <a href="#">Insurance Policy</a>
                        <a href="#">Careers</a>
                    </div>
                </div>
                <div class="col-md-2 col-sm-2 text-align-center">
                    <div class="angle-up-btn">
                        <a href="#top" class="smoothScroll wow fadeInUp" data-wow-delay="1.2s"><i
                                    class="fa fa-angle-up"></i></a>
                    </div>
                </div>
            </div>

        </div>
    </div>
</footer>

<!-- SCRIPTS -->
<script src="/web/js/jquery.js"></script>
<script src="/web/js/bootstrap.min.js"></script>
<script src="/web/js/jquery.sticky.js"></script>
<script src="/web/js/jquery.stellar.min.js"></script>
<script src="/web/js/wow.min.js"></script>
<script src="/web/js/smoothscroll.js"></script>
<script src="/web/js/owl.carousel.min.js"></script>
<script src="/web/js/custom.js"></script>
{{--<script type="module">--}}
{{--    // Import the functions you need from the SDKs you need--}}
{{--    import {initializeApp} from "https://www.gstatic.com/firebasejs/7.23.0/firebase-app.js";--}}
{{--    import {getAnalytics} from "https://www.gstatic.com/firebasejs/7.23.0/firebase-analytics.js";--}}
{{--    // TODO: Add SDKs for Firebase products that you want to use--}}
{{--    // https://firebase.google.com/docs/web/setup#available-libraries--}}

{{--    // Your web app's Firebase configuration--}}
{{--    // For Firebase JS SDK v7.20.0 and later, measurementId is optional--}}
{{--    const firebaseConfig = {--}}
{{--        apiKey: "AIzaSyARGtYAOlmjpPadAYUtG-qhagSyTxi9NkE",--}}
{{--        authDomain: "docapp-cd0cb.firebaseapp.com",--}}
{{--        projectId: "docapp-cd0cb",--}}
{{--        storageBucket: "docapp-cd0cb.appspot.com",--}}
{{--        messagingSenderId: "424311388981",--}}
{{--        appId: "1:424311388981:web:97f54a9c2e8d9ace286a7c",--}}
{{--        measurementId: "G-J1PMHWYWJL"--}}
{{--    };--}}

{{--    // Initialize Firebase--}}
{{--    const app = initializeApp(firebaseConfig);--}}
{{--    const analytics = getAnalytics(app);--}}
{{--</script>--}}
@stack('js')
</body>
</html>